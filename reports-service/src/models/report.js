import mongoose from 'mongoose'

const scopes = Object.freeze ({
    APPARTEMENT: 'appartement',
    IMMEUBLE: 'immeuble',
    COMMUNE: 'commune',
    AUTRES: 'autres'
})

const status = Object.freeze ({
    NON_TRAITE: 'non traité',
    EN_COURS_DE_TRAITEMENT: 'en cours de traitement',
    TRAITE: 'traité'
})

const categories = Object.freeze ({
    ELECTRICITE: 'électricité',
    COULOIR_PALIER: 'couloir/palier',
    ECLAIRAGE: 'éclairage',
    ASCENSEUR: 'ascenseur',
    PLOMBERIE: 'plomberie',
    JARDIN: 'jardin',
    AUTRES: 'autres'
})

const reportSchema = new mongoose.Schema({
    subject: {
        type: String,
        required: [true, 'Sujet est requis.']
    },
    description: {
        type: String
    },
    apartmentId: {
        type: String
    },
    address: {
        street: { type: String },
        city: { type: String },
        zipCode: { type: String },
        additional: {
            floor: Number,
            stair: String,
        }
    },
    scope: {
        type: String,
        enum: Object.values(scopes),
        required: [true, 'Scope est requis.']      
    },
    status: {
        type: String,
        enum: Object.values(status),
        default: status.NON_TRAITE,
        required: [true, 'Status est requis.']    
    },
    category: {
        type: String,
        enum: Object.values(categories),
        required: [true, 'Catégorie est requis.']  
    },
    isUrgent: {
        type: Boolean,
        default: false,
        required: [true, "isUrgent est requise"]
    },
    images: [{
        key: { type: Number, required: [true, "La clé de l'image est requise"]},
        order: { type: Number, required: [true, "L'ordre de l'image est requis"]}
    }],
    happenedAt: {
        type: Date,
    },
    endedAt: {
        type: Date
    },
    createdAt: {
        type: Date,
        default: Date.now,
        required: [true, 'Date de signalement est requis.']
    },
})
  
Object.assign(reportSchema.statics, {
    scopes, status, categories
})

export default mongoose.model('report', reportSchema)