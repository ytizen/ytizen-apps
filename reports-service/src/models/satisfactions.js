import mongoose from 'mongoose'

const scores = Object.freeze ({
    ONE: 1,
    TWO: 2,
    THREE: 3,
    FOUR: 4,
    FIVE: 5
})
const satisfactionSchema = new mongoose.Schema({

    reportId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'report',
        required : [true, 'Le numero de signalement est requis']
    },
    scores : {
        scoreQualityOfWork: {
            type: Number,
            enum: Object.values(scores),
            required: [true, 'La note de la qualité du travail est requis']
        },
        scoreDelayRespect: {
            type: Number,
            enum: Object.values(scores)
        },
        scoreProfessionalism: {
            type: Number,
            enum: Object.values(scores)
        }
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

Object.assign(satisfactionSchema.statics, {
    scores
})

export default mongoose.model('satisfaction', satisfactionSchema)