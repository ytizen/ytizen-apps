import express from 'express'
import cors from 'cors'
import settings from '../config/config'
import graphqlHTTP from 'express-graphql'
import schema from '../schemas/schema'
import dbConnect from '../config/db/mongo'
import report from '../models/report'
import satisfaction from '../models/satisfactions'

dbConnect()

const app = express()

app.use(cors())

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
    context: {report, satisfaction}
}));

const { appSettings : {port} } = settings

app.listen(port)
console.log(`Running GraphQL Reports API at localhost:${port}/graphql`)

