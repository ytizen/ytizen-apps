import mongoose from 'mongoose'
import settings from '../config'
 
const { dbSettings } = settings

let db = null

const dbConnect = MongoClientImpl => async () => {
    if (!db) {
        try {
            const mongoUri = `mongodb://${dbSettings.host}:${dbSettings.port}/${dbSettings.name}`
            let db = await MongoClientImpl.connect(
                mongoUri,
                { useNewUrlParser: true }
            )   
        } catch (err) {
            // todo: à envoyer à Sentry
            console.log(err)
        }
    }

    return db
}

export default dbConnect(mongoose)