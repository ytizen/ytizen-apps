import dotenv from 'dotenv'
dotenv.config()

const settings = {
    appSettings: {
        port: parseInt(process.env.APP_PORT) || 3000
    },
    dbSettings: {
        host: process.env.DB_HOST || 'ytizen-db',
        port: parseInt(process.env.DB_PORT) || 27017,
        name: process.env.DB_NAME || 'ytizen'
    }
}

export default settings