import { Types } from  'mongoose';

Types.ObjectId.prototype.valueOf = function () {
    return this.toString();
};

const Statisfaction = `
   
   type Satisfaction {
     id: ID
     reportId: String!
     scores: Score
     createdAt: Date 
   }
   type Score {
     scoreQualityOfWork: Int!
   }      
   extend type Query {
     Satisfactions(limit: Int, sortField: String, sortOrder: String): [Satisfaction]
     ScoresByReportIds(reportIds: [String]!, limit: Int, sortField: String, sortOrder: String): [Score]
   }
   extend type Mutation {
     createSatisfaction(reportId: String!, scoreQualityOfWork: Int!): Satisfaction
   }
`
export default ()=> [Statisfaction]