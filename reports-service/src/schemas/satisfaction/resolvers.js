import { statisfactionService } from '../../services';


export const StatisfactionQuery = {
    Satisfactions: (_, {limit, sortField, sortOrder}, context) =>
        statisfactionService.getAllStatisfaction({limit, sortField, sortOrder}, context.satisfaction),
    ScoresByReportIds: (_, {ids}, context) =>
        statisfactionService.getScoresByReportIds(ids, context.satisfaction),
}

export const StatisfactionMutation = {
    createSatisfaction: (_, {reportId, scoreQualityOfWork}, context) =>
        statisfactionService.createStatisfaction({reportId, scoreQualityOfWork}, context.satisfaction)
}