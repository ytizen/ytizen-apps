import { makeExecutableSchema } from 'graphql-tools'
import Report from './report/schema'
import Satisfaction from './satisfaction/schema';
import resolvers from './resolvers'

const schema = makeExecutableSchema({
    typeDefs: [Report, Satisfaction],
    resolvers,
    logger: { log: e => console.log(e) },
})

export default schema