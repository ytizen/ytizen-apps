import {
    Query as ReportQuery,
    Mutation as ReportMutation,
    Report,
} from './report/resolvers'
import { StatisfactionQuery ,StatisfactionMutation } from './satisfaction/resolvers'

//import Date from './scalar/Date'

const resolvers = {
    Query: {
        ...ReportQuery,
        ...StatisfactionQuery
    },
    Mutation: {
        ...ReportMutation,
        ...StatisfactionMutation
    },
    Report,
  //  Date,
}

export default resolvers