const Report = `
    type Report {
        id: ID!
        subject: String!
        description: String
        apartmentId: String
        address: Address
        scope: String!
        status: String!
        category: String!
        isUrgent: Boolean
        happenedAt: Date
        endedAt: Date
        createdAt: Date
    },

    type Address {
        street: String
        city: String
        zipCode: String
        additional: AdditionalAddress
    }

    type AdditionalAddress {
        floor: Int
        stair: String
    }
    
    scalar Date

    type Query {
        Report(id: ID!): Report
        Reports(limit: Int, sortField: String, sortOrder: String): [Report]
    }

    type Mutation {
        createReport(subject: String!, description: String, scope: String!, category: String!): Report
    }
`

export default () => [Report]