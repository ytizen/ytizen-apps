const reports = [
    { 
        id: 1, 
        subject: 'Une ampoule est grillée', 
        description: 'Une ampoule est grillée', 
        apartmentId: 10, 
        "address" : {
            "street" : "3 rue Anatole",
            "city" : "Paris",
            "zipCode" : "75010",
            "additional" : {
                "floor": "0"
            }
        },
        "scope" : "appartement", 
        "status" : "non traité", 
        "category" : "éclairage", 
        "isUrgent" : true,
        happenedAt: new Date(), 
    },
    { 
        id: 2, 
        subject: 'L\'ascenceur est en panne', 
        description: 'L\'ascenceur est en panne car ils sont montés à plusieurs', 
        apartmentId: 10, 
        "address" : {
            "street" : "3 rue Anatole",
            "city" : "Paris",
            "zipCode" : "75010",
            "additional" : {
                "floor": "0"
            }
        },
        "scope" : "immeuble", 
        "status" : "non traité", 
        "category" : "éclairage", 
        "isUrgent" : true,
        happenedAt: new Date(), 
    },
]

export const Query = {
    Reports: (_, __, context) => context.report.find({}),
    Report: (_, {id}) => reports.find(report => report.id == id)
}

export const Mutation = {
    createReport: (_, {subject, description, scope, category}) => {
        // todo: create Report
    }
}

export const Report = {}
