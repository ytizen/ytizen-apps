export const statisfactionService = {
    getAllStatisfaction,
    getScoresByReportIds,
    createStatisfaction
}


function getAllStatisfaction(options, model) {
  return model.find(options);
}

function getScoresByReportIds(reportIds, model) {
    const ids =  reportIds.map(id => ObjectId(id));
    return model.find({reportId: {$in: ids}});
}

async function  createStatisfaction(inputData, model) {
       const data = {
           reportId: inputData.reportId,
           scores : {
               scoreQualityOfWork : inputData.scoreQualityOfWork
           }
       }
      const result = await  model.create(data);
      return result;
}