import React, { Component } from 'react';
import { Router, Route, Switch } from "react-router-dom";
import Home from "layouts/Home/Home.jsx";
import ProtectedRoutes from "components/Private/ProtectedRoutes.jsx";
import Login from 'layouts/Login/Login.jsx';
import { history } from "./_helpers/history";



export class App extends  Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
           <Router history={history}>
             <Switch>
                 <Route exact path='/login' component={Login}/>
                 <ProtectedRoutes  path='/' component={Home}/>
             </Switch>
           </Router>
        );
    }

}

