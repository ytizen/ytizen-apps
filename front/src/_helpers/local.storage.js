
export const saveState = function(key, state) {
    try {
        if(state){
          const serializeState = JSON.stringify(state);
          localStorage.setItem(key, serializeState);
        }
    }catch (e) {

    }

}

export const getState = function (key) {
    try {
        const state = localStorage.getItem(key);
        if(state) {
            return JSON.parse(state);
        }
        return undefined;
    }catch (e) {
        return undefined
    }
}

export const removeItem = function(key) {

    localStorage.removeItem(key);
}