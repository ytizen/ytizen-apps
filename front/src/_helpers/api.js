import axios from 'axios';
import ApolloClient from "apollo-boost";

export const API =  axios.create({
    baseURL: `http://localhost:4000/api`
});

export const clientGraphql = new ApolloClient({
    uri: "http://localhost:3000/graphql"
});