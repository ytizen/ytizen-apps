import { reportsConstants } from '../_constants'

const initialState = {
    message : undefined,
    data : [],
    isFetching: false
}

export function reports(state = initialState, action) {

  switch (action.type) {
      case reportsConstants.GET_ALL_REQUEST :
          return Object.assign({}, state, {
              isFetching: true
       })
      case reportsConstants.GET_ALL__SUCCESS :
         return Object.assign({}, state, {
              isFetching: false,
              data: action.data,
              message: undefined
         })
      case reportsConstants.GET_ALL_FAILURE :
          return Object.assign({}, state, {
              isFetching: false,
              message: action.error
          })
      default :
          return state;
  }
}