import { userConstants } from '../_constants';
import { getState  } from '../_helpers/local.storage';

const userFromLocalStorage = getState('user');

const initialState = {
    user: userFromLocalStorage ? userFromLocalStorage : {},
    message: '',
    isFetching: false
}


export function authentification(state = initialState, action) {

    switch (action.type) {
        case userConstants.LOGIN_REQUEST :
          return Object.assign({}, state, {
              isFetching: true
          });
        case userConstants.LOGIN_SUCCESS :
            return Object.assign({}, state, {
                user: action.user,
                isFetching : false,
                message: undefined
            })
        case userConstants.LOGIN_FAILURE :
            return Object.assign({}, state, {
                message : action.error,
                isFetching : false
            })
        default:
            return state;
    }

}