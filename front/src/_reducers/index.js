import { combineReducers } from 'redux';

import { alert } from './alert.reducer';
import { authentification } from './auth.reducer';

import { reports } from './reports.reducer';


const rootReducer = combineReducers({
    authentification,
    reports,
    alert
})

export  default rootReducer;