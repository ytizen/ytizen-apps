import React from "react";
import { Provider } from 'react-redux';
import { render } from "react-dom";
import "assets/css/material-dashboard-react.css?v=1.5.0";
import { App } from "./App";
import { store, configureFakeBackend } from './_helpers';

window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

configureFakeBackend();

render(
 <Provider store={store}>
  <App/>
 </Provider>,
  document.getElementById("root")
);
