import gql from "graphql-tag";
import { clientGraphql } from '../_helpers'


export const reportsService = {
    getReports
}


function getReports() {
   return clientGraphql
        .query({
            query: gql`
              {
                Reports {    
                   subject,
                   description,
                   scope,
                   status,
                   category
                 }
              }
            `
        });

   /*
    {
                Reports {
                   id,
                   subject,
                   description,
                   apartmentId,
                   address {
                     street,
                     city
                   },
                   scope,
                   status,
                   category,
                   isUrgent,
                   happenedAt
                 }
              }
    */
}