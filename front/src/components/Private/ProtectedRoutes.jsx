import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const ProtectedRoutes = ({ component: Component, ...props }) => (
    !props.authentification.user.token ? <Redirect to='/login' from = {props.location.path}/>
               : <Component {...props} />
);

const mapStateToProps = ({authentification}) => ({
    authentification
});

export default connect(mapStateToProps)(ProtectedRoutes);