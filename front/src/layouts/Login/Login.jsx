import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../_actions/index';

// core components
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import withStyles from "@material-ui/core/styles/withStyles";

import image from "assets/img/cover.jpeg"

import {saveState} from '../../_helpers/local.storage';

const styles = theme => ({
    wrapper : {
        minHeight: '100vh',
        width: '100%',
        display: 'flex',
        backgroundImage: `linear-gradient(0deg,rgba(44,44,44,.2),#00acc169), url(${image})`,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundSize: 'cover',
        backgroundRepeat: 'repeat',
    },
    container : {
        margin: '0px auto',
        textAlign: 'center',
        marginTop: '50px',
        width: 'calc(100% - 24px)',
        maxWidth: '496px'
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    cardFooter: {
        margin: 'auto'
    }
});


class Login extends Component {


    constructor(props){
       super(props)

       this.state = { email: '', password : '', submitted: false}
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        const user  = { id: 1, email : 'admin@test.com', password: 'admin', firstName: 'Test', lastName: 'dddddd' };
        saveState('user', user);
    }

    handleChange= name => event => {
        this.setState({
            [name]: event.target.value,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({submitted : true});
        const { email, password }  = this.state;
        const { dispatch } = this.props;
        if(email && password) {
            dispatch(userActions.login(email, password));
        }
    }
    render(){
        const { email, password, submitted } = this.state;
        const { classes, alert } = this.props;
        return (
             <div className={classes.wrapper}>
                <Grid className={classes.container}>
                    <GridItem>
                      <form  noValidate onSubmit={this.handleSubmit}>
                        <Card>
                            <CardHeader color="info">
                             <div>
                                <h2>Ytizen</h2>
                                 {alert.message &&
                                        <div className={`alert ${alert.type}`}>{alert.message}</div>}
                                </div>
                            </CardHeader>
                            <CardBody>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <CustomInput
                                            className={classes.textField}
                                            labelText="Email"
                                            id="email"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange:this.handleChange('email'),
                                                type:'email',
                                                value:email
                                            }}
                                        />
                                        {submitted && !email &&
                                            <div className="help-block">L'email est obligatoire</div>
                                        }
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <CustomInput
                                            labelText="Mot de passe"
                                            id="password"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange:this.handleChange('password'),
                                                type:'password',
                                                value:password
                                            }}
                                            className={classes.textField}
                                        />
                                        {submitted && !password &&
                                        <div className="help-block">Le mot de passe est obligatoire</div>
                                        }
                                    </GridItem>
                                </GridContainer>
                            </CardBody>
                            <CardFooter className={classes.cardFooter}>
                                <Button type="submit"  color="info">Connexion</Button>
                            </CardFooter>
                         </Card>
                        </form>
                    </GridItem>
                </Grid>
             </div>

        )
    }


}

const mapStateToProps = ({alert}) => (
    {alert}
)


export default withStyles(styles) (connect(mapStateToProps)(Login))