import { reportsService } from "../_services";
import { reportsConstants } from "../_constants";
import { alertActions } from "../_actions";

export const reportsActions = {
    getAll
}

function getAll() {
    return dispatch => {
        dispatch(request());
        reportsService.getReports()
            .then(
                res => {
                    dispatch(success(res.data.Reports));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: reportsConstants.GET_ALL_REQUEST } }
    function success(data) { return { type: reportsConstants.GET_ALL__SUCCESS, data } }
    function failure(error) { return { type: reportsConstants.GET_ALL__SUCCESS, error } }

}