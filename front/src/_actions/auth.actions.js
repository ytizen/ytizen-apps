import { userConstants }  from '../_constants'
import { userService } from '../_services/user.service';
import { alertActions } from '../_actions/alert.actions';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout
}

function login(email, password) {
    return dispatch => {
        dispatch(request({ password }));
        userService.login(email, password)
            .then(
                user => {
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }

}

function logout() {
    return { type: userConstants.LOGOUT };
}