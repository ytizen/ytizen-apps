# Ytizen Apps

All apps to create ytizen software

## Pré requis

* Installer [docker](https://www.docker.com/) et [docker-compose](https://docs.docker.com/compose/)

## Installation de l'application

* git clone git@gitlab.com:ytizen/ytizen-apps.git

* sur la branche 'develop' et à la racine du projet, lancer docker : docker-compose up (ajouter l'option '-d' pour démarrer les images en tache de fond)

* pour chaque service (reports-service,...), créer le fichier .env en se basant sur le template .env.example et modifier éventuellement vos variables d'environnements

* Ajouter dans vos /ect/hosts:  **127.0.0.1 admin.ytizen.local**

* Accéder à l'application via **admin.ytizen.local:3000**

* Accéder au front via **admin.ytizen.local:4200**

* Commande utile : docker ps -a pour consulter les ports utilisés par les différentes images

## Patterns, principes, technologies notables utilisés

* Microservices

* Service instance per container : chaque microservice, service de stockage doit est containerisé avec Docker

* Private-tables-per-service : chaque service manipule un ensemble de tables (documents en l'occurence) qui ne sont accessibles uniquement que par ce service


## Problèmes connus

* Lorsque vous rebuilder les images Docker, à la suite d'un docker-compose up -d et que vous rencontrer l'erreur "permission denied" sur le fichier .config => chmod 777 -R .config. Ce fichier est crée par nodemon.